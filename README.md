# Stash WebVR

A stupid simple Stash client made for just for VR videos

## Getting Started

1. Run `npm install`
2. Modify the `./src/config.js` to match your machine's configuration
3. Run the client app by running `npm start` and the server with `node proxy.mjs`
4. The server can also serve the web app on the same port. But you need to first run `npm run build` to generate a build (`./build`) and then restart your server with `node proxy.mjs`.

## Adding content

Adding a video to Stash WebVR is as easy as tagging a video as "Virtual Reality" in Stash. Yea thats it.

## Does this work with DeoVR or PlayaVR?

Nah, but it will soon.
