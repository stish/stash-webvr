import { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import config from "./config.mjs";

const PlayerOverlay = ({ scene, handleClose, openVideo }) => {
  const modalRef = useRef(null);
  const [playerDims, _playerDims] = useState(null);
  const [hoverOnVideo, _hoverOnVideo] = useState(false);
  const [displayVideo, _displayVideo] = useState(false);
  useEffect(() => {
    if (modalRef.current?.clientWidth) {
      let { clientWidth, clientHeight } = modalRef.current;
      _playerDims({
        height: scene.file.height,
        width: scene.file.width,
        modalWidth: clientWidth,
        modalHeight: clientHeight,
      });
    }
  }, [modalRef]);

  useEffect(() => {
    const handleResize = () => {
      if (modalRef.current?.clientWidth) {
        let { clientWidth, clientHeight } = modalRef.current;
        _playerDims({
          height: scene.file.height,
          width: scene.file.width,
          modalWidth: clientWidth,
          modalHeight: clientHeight,
        });
      }
    };
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, [scene]);

  useEffect(() => {
    const handleEsc = (e) => {
      if (e.key === "Escape") {
        handleClose();
      }
    };
    window.addEventListener("keydown", handleEsc);
    return () => window.removeEventListener("keydown", handleEsc);
  }, [handleClose]);

  return (
    <OverlayWrapper>
      <ModalWrapper>
        <Backdrop onClick={handleClose} />
        <Modal
          ref={modalRef}
          hoverOnVideo={hoverOnVideo}
          style={
            playerDims && {
              maxHeight:
                (playerDims.modalWidth * playerDims.height) / playerDims.width +
                100,
            }
          }
        >
          <SceneTitle>{scene.title}</SceneTitle>
          <SceneDetails>{scene.details}</SceneDetails>
          {playerDims ? (
            displayVideo ? (
              <VideoPlayer
                allow="autoplay; fullscreen; picture-in-picture; xr-spatial-tracking; clipboard-write"
                onMouseOver={() => _hoverOnVideo(true)}
                onMouseLeave={() => _hoverOnVideo(false)}
                frameBorder={0}
                style={{
                  height:
                    (playerDims.modalWidth * playerDims.height) /
                    playerDims.width,
                }}
                title="video plauer"
                src={`http://${config.stash.hostname}:${config.stash.port}/scene/${scene.id}/stream`}
              />
            ) : (
              <>
                <PlayButton
                  onClick={() =>
                    openVideo(
                      `http://${config.stash.hostname}:${config.stash.port}/scene/${scene.id}/stream`
                    )
                  }
                >
                  Watch Now
                </PlayButton>
                <PlayInlineButton onClick={() => _displayVideo(true)}>
                  Play Video Inline
                </PlayInlineButton>
              </>
            )
          ) : (
            <span>loading scene dimensions</span>
          )}
          {scene?.performers?.length > 0 && (
            <DataRow>
              <label>Performers:</label>
              {scene?.performers?.map((performer, k) => (
                <PerformerCard
                  key={k}
                  name={performer.name}
                  image={`http://${config.proxy.hostname}:${config.proxy.port}/img?src=${performer.image_path}`}
                />
              ))}
            </DataRow>
          )}
          {scene?.tags?.length > 0 && (
            <DataRow>
              <label>Tags:</label>
              {scene?.tags?.map((tag, k) => (
                <TagButton key={k}>{tag.name}</TagButton>
              ))}
            </DataRow>
          )}
        </Modal>
      </ModalWrapper>
    </OverlayWrapper>
  );
};

const PlayInlineButton = styled.a`
  color: white;
  text-decoration: underline;
  margin-left: 10px;
  cursor: pointer;
`;

const PlayButton = styled.button`
  height: 50px;
  min-width: 100px;
  border: none;
  background-color: orange;
  font-size: 16px;
  font-weight: bold;
  padding: 0px 25px;
  border-radius: 10px;
  cursor: pointer;
  drop-shadow: 0px 0px 5px #ff8c00;
  &:hover {
    background-color: #ff8c00;
  }
`;

const DataRow = styled.div`
  label {
    display: block;
    height: 40px;
    margin: 20px 10px 0 10px;
    font-weight: bold;
  }
`;

const TagButton = styled.button`
  padding: 5px 10px;
  margin: 10px;
  border-radius: 15px;
  border: 0;
  background-color: #333;
  color: #fff;
  font-size: 1.2rem;
`;

const PerformerCard = styled.div`
  background-image: url(${(props) => props.image});
  background-size: cover;
  background-position: center center;
  width: 100px;
  height: 150px;
  display: inline-block;
  position: relative;
  margin: 10px;
  &:before {
    content: "${(props) => props.name}";
    position: absolute;
    bottom: 15px;
    left: 50%;
    transform: translateX(-50%);
  }
`;

const SceneDetails = styled.p`
  margin-top: 10px;
  padding: 0;
  margin-bottom: 20px;
  ellipsis: true;
  max-width: 800px;
  text-overflow: hidden;
  white-space: break-spaces;
`;
const SceneTitle = styled.h1`
  margin: 0;
  //   margin-left: 20px;
  margin-top: 20px;
  padding: 0;
  max-width: calc(100% - 100px);
  overflow: hidden;

  display: inline-block;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

const VideoPlayer = styled.iframe`
  width: 100%;
  height: 100%;
  cursor: pointer;
`;

const Modal = styled.div`
  color: white;
  min-width: 900px;
  max-width: 900px;
  transition: all 200ms linear;
  z-index: 4;
  transition: all 200ms linear;
  @media screen and (max-width: 900px) {
    min-width: unset;
    width: 100vw;
    transition: all 200ms linear;
  }
`;

const ModalWrapper = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  display: flex;
  justify-content: center;
  padding-top: 40px;
  //   align-items: center;
`;

const Backdrop = styled.div`
  cursor: pointer;
  background-color: rgba(0, 0, 0, 0.9);
  backdrop-filter: blur(5px);
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0px;
  left: 0px;
`;

const OverlayWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0px;
  left: 0px;
  z-index: 3;
  overflow: scroll;
`;

export default PlayerOverlay;
