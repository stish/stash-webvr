const config = {
  // This is the built-in server (proxy.mjs)
  proxy: {
    // Replace this with your machines IP address
    hostname: "192.168.127.185",
    // No need to change this
    port: 8918,
  },
  // This is your local Stash server
  stash: {
    // Replace this with your machines IP address
    hostname: "192.168.127.185",
    // Stash's default port is 9999
    port: 9999,
  },
};

export default config;
