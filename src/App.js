import { useEffect, useState } from "react";
import styled from "styled-components";
import config from "./config.mjs";
import PlayerOverlay from "./PlayerOverlay.js";

const mockData = () => ({
  data: {
    findScenes: {
      scenes: [...new Array(40)].map((_, i) => ({
        id: i,
        title: "image_" + i + ".jpg",
        details: "",
        paths: {
          screenshot: "https://picsum.photos/200/300?random=" + i,
          preview: "https://picsum.photos/200/300?random=" + i,
        },
        file: {
          duration: 90,
        },
      })),
    },
  },
});

// let sfwMode = window.location.hash === "#sfw";
const SafeModeButton = styled.button`
  position: absolute;
  top: 0px;
  left: 0px;
  background-color: orange;
  border: none;
  border-radius: 15px;
  padding: 10px;
  display: none;
`;

const AppContainer = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  width: 100vw;
  height: 100vh;
  overflow: scroll;
  background-color: #000;

  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`;

const ScenesContainer = styled.div``;

const SceneBlock = styled.div`
  height: 220px;
  width: 300px;
  display: inline-block;
  background-color: #222;
  color: white;
  word-wrap: break-word;
  float: left;
  margin: 10px;
  border-radius: 10px;
  overflow: hidden;
`;
const PreviewContainer = styled.div`
  height: 160px;
  width: 100%;
  position: relative;
  overflow: hidden;
`;
const ScreenShot = styled.div`
  z-index: 2;
  position: absolute;
  top: 0px;
  left: 0px;
  height: 160px;
  width: 100%;
  background-image: url(${(props) => props?.src});
  background-size: cover;
  background-position: center center;
  cursor: pointer;
`;

const VideoPreview = styled.video`
  z-index: 1;
  position: absolute;
  top: -70px;
  left: 0px;
  // height: 160px;
  width: 200%;
`;

const ScreenTitle = styled.span`
  display: flex;
  justify-content: center;
  align-content: center;
  align-items: center;
  width: 100%;
  height: 60px;
  text-align: center;
  font-weight: bold;
  word-wrap: break-word;
  overflow: hidden;
`;

const Badges = styled.span`
  position: absolute;
  bottom: 10px;
  z-index: 2;
  right: 5px;
  span {
    margin-left: 5px;
    background-color: rgba(0, 0, 0, 0.5);
    padding: 5px;
    border-radius: 5px;
    font-weight: bold;
    font-size: 12px;
    color: white;
  }
`;

const formatDuration = (scene) => {
  let dur = scene.file.duration;
  if (!dur) return "";
  let value = dur;
  let hour = 0;
  let minute = 0;
  let seconds = 0;
  if (value >= 3600) {
    hour = Math.floor(value / 3600);
    value -= hour * 3600;
  }
  minute = Math.floor(value / 60);
  value -= minute * 60;
  seconds = value;

  const res = [
    minute.toString().padStart(2, "0"),
    seconds.toString().padStart(2, "0"),
  ];
  if (hour) res.unshift(hour.toString());
  return res.join(":").split(".")[0];
};

const formatResolutionFriendlyName = (scene) => {
  if (scene.file.width >= 6144 && scene.file.height >= 3240) {
    return "6k";
  } else if (scene.file.width >= 3840 && scene.file.height >= 2160) {
    return "4K";
  } else if (scene.file.width >= 1920 && scene.file.height >= 1080) {
    return "1080P";
  } else if (scene.file.width >= 1080 && scene.file.height >= 720) {
    return "720p";
  } else if (scene.file.width >= 720 && scene.file.height >= 480) {
    return "480p";
  } else if (scene.file.width >= 470 && scene.file.height >= 320) {
    return "320p";
  } else {
    return false;
  }
  // return "4k" or "6k" or "1080p" or "720p" from scene.file.width and scene.file.height
};

const makeToolTip = (scene) =>
  `${scene.title}${scene.details && `\n\n${scene.details}`}`;

const Block = ({ scene, openVideo, handleSelectScene }) => {
  let [showVideoPreview, _showVideoPreview] = useState(false);

  const handleVideoMounted = (element) => {
    if (element !== null) {
      element.currentTime = 30;
    }
  };
  return (
    <SceneBlock key={scene.id} title={makeToolTip(scene)}>
      <ScreenTitle>{scene.title.replace(".mp4", "")}</ScreenTitle>
      <PreviewContainer>
        <ScreenShot
          onMouseOver={() => _showVideoPreview(true)}
          onMouseLeave={() => _showVideoPreview(false)}
          onClick={() =>
            // openVideo(
            //   `http://${config.stash.hostname}:${config.stash.port}/scene/${scene.id}/stream`
            // )
            handleSelectScene(scene)
          }
          src={
            !showVideoPreview
              ? `http://${config.proxy.hostname}:${config.proxy.port}/img?src=${scene?.paths?.screenshot}`
              : null
          }
          alt={scene.title}
        />
        {showVideoPreview && (
          <VideoPreview autoPlay={true} ref={handleVideoMounted} muted>
            <source
              src={`http://${config.proxy.hostname}:${config.proxy.port}/img?src=${scene?.paths?.preview}`}
              type="video/mp4"
            />
          </VideoPreview>
        )}

        {
          <Badges>
            {formatResolutionFriendlyName(scene) && (
              <span className="resolution">
                {formatResolutionFriendlyName(scene)}
              </span>
            )}
            <span className="duration">{formatDuration(scene)}</span>
          </Badges>
        }
      </PreviewContainer>
    </SceneBlock>
  );
};

function App() {
  let [sfwMode, _sfwMode] = useState(true);
  let [data, _data] = useState(null);
  let [selectedScene, _selectedScene] = useState(null);
  useEffect(() => {
    if (!sfwMode || !data) {
      if (!sfwMode) {
        fetch(`http://${config.proxy.hostname}:${config.proxy.port}/scenes`)
          .then((response) => response.json())
          .then((d) => _data(d))
          .catch((error) => console.error(error));
      } else {
        _data(mockData());
      }
    } else {
      _data(mockData());
    }
  }, [sfwMode]);

  useEffect(() => {
    const handleS = (e) => {
      if (e.key === "s" || e.key === "S" || e.key === " ") {
        e.preventDefault();
        _sfwMode(!sfwMode);
        _selectedScene(null);
      }
    };
    window.addEventListener("keydown", handleS);
    return () => window.removeEventListener("keydown", handleS);
  }, [sfwMode]);

  const openVideo = (stream) => {
    window.open(stream, "_blank");
  };
  return (
    <AppContainer>
      <SafeModeButton onClick={() => _sfwMode(!sfwMode)}>
        {sfwMode ? "NSFW" : "SFW"}
      </SafeModeButton>
      {selectedScene && (
        <PlayerOverlay
          scene={selectedScene}
          handleClose={() => _selectedScene(null)}
          openVideo={openVideo}
          sfwMode={sfwMode}
        />
      )}
      {data?.data?.findScenes?.scenes && (
        <ScenesContainer>
          {data?.data?.findScenes?.scenes.map((scene, k) => (
            <Block
              scene={scene}
              openVideo={openVideo}
              key={scene?.id}
              handleSelectScene={_selectedScene}
              sfwMode={sfwMode}
            />
          ))}
        </ScenesContainer>
      )}
    </AppContainer>
  );
}

export default App;
