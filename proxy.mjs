import fetch from "node-fetch";
import express from "express";
import http from "http";
import cors from "cors";
import bodyParser from "body-parser";
import config from "./src/config.mjs";

var port = 8918;
var app = express();
var server = http.createServer(app);
app.use(cors());
app.use(bodyParser());
// app.enable("etag");

// This should make sure bodies aren't cached
// so the streaming tests always pass
app.use(function (req, res, next) {
  res.setHeader("Cache-Control", "no-store");
  next();
});

// static files
app.use(express.static("build"));

app.get("/img", (req, res) => {
  let src = req.query.src || null;
  if (src) {
    fetch(src)
      .then((response) => response.buffer())
      .then((buffer) => {
        res.setHeader("Content-Type", "image/png");
        res.setHeader("Content-Length", buffer.length);
        res.send(buffer);
      });
  } else {
    res.send("No src provided");
  }
});

app.get("/scenes", (req, res) => {
  fetch(`http://${config.stash.hostname}:${config.stash.port}/graphql`, {
    method: "POST",
    mode: "cors",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      operationName: "FindScenes",
      variables: {
        filter: {
          page: 1,
          per_page: 1000,
          sort: "date",
          direction: "ASC",
        },
        scene_filter: {
          tags: {
            value: ["439"],
            modifier: "INCLUDES_ALL",
            depth: 0,
          },
        },
      },
      query:
        "query FindScenes($filter: FindFilterType, $scene_filter: SceneFilterType, $scene_ids: [Int!]) {\n  findScenes(filter: $filter, scene_filter: $scene_filter, scene_ids: $scene_ids) {\n    count\n    filesize\n    duration\n    scenes {\n      ...SceneData\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment SceneData on Scene {\n  id\n  checksum\n  oshash\n  title\n  details\n  url\n  date\n  rating\n  o_counter\n  organized\n  path\n  phash\n  interactive\n  interactive_speed\n  captions {\n    language_code\n    caption_type\n    __typename\n  }\n  created_at\n  updated_at\n  file {\n    size\n    duration\n    video_codec\n    audio_codec\n    width\n    height\n    framerate\n    bitrate\n    __typename\n  }\n  paths {\n    screenshot\n    preview\n    stream\n    webp\n    vtt\n    chapters_vtt\n    sprite\n    funscript\n    interactive_heatmap\n    caption\n    __typename\n  }\n  scene_markers {\n    ...SceneMarkerData\n    __typename\n  }\n  galleries {\n    ...SlimGalleryData\n    __typename\n  }\n  studio {\n    ...SlimStudioData\n    __typename\n  }\n  movies {\n    movie {\n      ...MovieData\n      __typename\n    }\n    scene_index\n    __typename\n  }\n  tags {\n    ...SlimTagData\n    __typename\n  }\n  performers {\n    ...PerformerData\n    __typename\n  }\n  stash_ids {\n    endpoint\n    stash_id\n    __typename\n  }\n  sceneStreams {\n    url\n    mime_type\n    label\n    __typename\n  }\n  __typename\n}\n\nfragment SceneMarkerData on SceneMarker {\n  id\n  title\n  seconds\n  stream\n  preview\n  screenshot\n  scene {\n    id\n    __typename\n  }\n  primary_tag {\n    id\n    name\n    aliases\n    __typename\n  }\n  tags {\n    id\n    name\n    aliases\n    __typename\n  }\n  __typename\n}\n\nfragment SlimGalleryData on Gallery {\n  id\n  checksum\n  path\n  title\n  date\n  url\n  details\n  rating\n  organized\n  image_count\n  cover {\n    file {\n      size\n      width\n      height\n      __typename\n    }\n    paths {\n      thumbnail\n      __typename\n    }\n    __typename\n  }\n  studio {\n    id\n    name\n    image_path\n    __typename\n  }\n  tags {\n    id\n    name\n    __typename\n  }\n  performers {\n    id\n    name\n    gender\n    favorite\n    image_path\n    __typename\n  }\n  scenes {\n    id\n    title\n    path\n    __typename\n  }\n  __typename\n}\n\nfragment SlimStudioData on Studio {\n  id\n  name\n  image_path\n  stash_ids {\n    endpoint\n    stash_id\n    __typename\n  }\n  parent_studio {\n    id\n    __typename\n  }\n  details\n  rating\n  aliases\n  __typename\n}\n\nfragment MovieData on Movie {\n  id\n  checksum\n  name\n  aliases\n  duration\n  date\n  rating\n  director\n  studio {\n    ...SlimStudioData\n    __typename\n  }\n  synopsis\n  url\n  front_image_path\n  back_image_path\n  scene_count\n  scenes {\n    id\n    title\n    path\n    __typename\n  }\n  __typename\n}\n\nfragment SlimTagData on Tag {\n  id\n  name\n  aliases\n  image_path\n  __typename\n}\n\nfragment PerformerData on Performer {\n  id\n  checksum\n  name\n  url\n  gender\n  twitter\n  instagram\n  birthdate\n  ethnicity\n  country\n  eye_color\n  height\n  measurements\n  fake_tits\n  career_length\n  tattoos\n  piercings\n  aliases\n  favorite\n  ignore_auto_tag\n  image_path\n  scene_count\n  image_count\n  gallery_count\n  movie_count\n  tags {\n    ...SlimTagData\n    __typename\n  }\n  stash_ids {\n    stash_id\n    endpoint\n    __typename\n  }\n  rating\n  details\n  death_date\n  hair_color\n  weight\n  __typename\n}\n",
    }),
  })
    .then((response) => response.json())
    .then((data) => res.send(data));
});

server.listen(port);
